<?php get_header(); ?>
<div id="main-error">
<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
				<div id="error-txt">
					<h3>抱歉! 此頁不存在!</h3>
					<p><a href="http://203.75.56.18/"><i class="fa fa-arrow-left" aria-hidden="true"></i> 返回首頁</a></p>


					</div>

				<article id="post-0" <?php post_class( 'et_pb_post not_found' ); ?>>
					<?php get_template_part( 'includes/no-results', '404' ); ?>
				</article> <!-- .et_pb_post -->

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->
</div>

<?php get_footer(); ?>
