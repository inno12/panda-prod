<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'panda');

/** MySQL database username */
define('DB_USER', 'panda');

/** MySQL database password */
define('DB_PASSWORD', 'panda123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$*OSd-%ZNaA,UbOP!i|5<iQH=nH|_Da^<j`~EP8E>7^waXQlu[vHsAB-{7NnE2Bp');
define('SECURE_AUTH_KEY',  '7|Z:<T?bN!<H{Q:;NPTJRE]Pf^[N7M]p!XLm`%5 !bw&yhScmMdF$gGjQo%LlUN:');
define('LOGGED_IN_KEY',    'RuSO02py.9Q8V@vPaKMpoFo.%CX+U~j?D0g/k?j?{/zM1.5.V2&2 )eqaF<05PE}');
define('NONCE_KEY',        'MKTYo^N2nl;c@F~0wGEl^>Tz>;+eq$~A {OLwS}tx!W`3KYNDf?5Mh3/G]#_EOWI');
define('AUTH_SALT',        '*hh2q-A(GC/6a:9<A1E*pNq E.v|M|S|g3 N!@Z06mcD9ja`~q.z{1G?6|]DFp*[');
define('SECURE_AUTH_SALT', 'V-RBV)$5y:Lr0Fgl{A|-5_-!)Qzcay,8H$|py6~}7mC8rdMY=.-;-Rx1 m Ukm(9');
define('LOGGED_IN_SALT',   '#au.Ku<aDyVu@hQ{wIsqqBDHx-n.f- y/A[_7&LGv!ojOS!k!ALe`YZeTOOsUs|h');
define('NONCE_SALT',       '5,p+DzXm:N$yr=dJ2 gHC1?*p1Z?u}/JF:pYc#l?nzSK0M[-[4Q|%Ja*5Q6lL9qU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
